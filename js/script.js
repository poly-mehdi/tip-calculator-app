// Get the input fields
const billInput = document.getElementById('amount')
const tipInputs = document.querySelectorAll('.tip-percentage')
const peopleInput = document.getElementById('people')

// Get the custom tip input
const customTip = document.getElementById('custom')

// Get the output fields
const tipAmount = document.getElementById('tip-amount')
const totalAmount = document.getElementById('total-amount')

// Get the reset button
const resetButton = document.getElementById('reset')

// Set tip to 0
let tip = 0

billInput.addEventListener('input', () => {
  resetButton.classList.add('btn-active')
})
peopleInput.addEventListener('input', () => {
  resetButton.classList.add('btn-active')
})

// Add event listeners to the tip inputs
tipInputs.forEach((tipInput) => {
  tipInput.addEventListener('click', () => {
    tipInputs.forEach((otherTip) => {
      if (otherTip !== tipInput && !otherTip.classList.contains('custom')) {
        otherTip.classList.remove('active')
      } else if (otherTip.classList.contains('custom')) {
        otherTip.value = ''
      }
    })

    tipInput.classList.add('active')
    tip = parseInt(tipInput.innerHTML.slice(0, -1))
    calculateTip()
  })
})

// Add event listener to the custom tip input
customTip.addEventListener('input', () => {
  resetButton.classList.add('btn-active')
  tipInputs.forEach((otherTip) => {
    if (otherTip !== customTip) {
      otherTip.classList.remove('active')
    }
    console.log(customTip.value)
    tip = parseInt(customTip.value)
    calculateTip()
  })
})

// Calculate the tip and total amounts
const calculateTip = () => {
  if (
    billInput.value === '' ||
    peopleInput.value === '' ||
    peopleInput.value === '0'
  ) {
  } else {
    const bill = parseInt(billInput.value)
    const people = parseInt(peopleInput.value)

    const tipAmountValue = bill * (tip / 100)
    const tipAmountValuePerson = tipAmountValue / people
    const totalAmountValue = (bill + tipAmountValue) / people
    tipAmount.innerHTML = `$${tipAmountValuePerson.toFixed(2)}`
    totalAmount.innerHTML = `$${totalAmountValue.toFixed(2)}`
  }
}

// Reset all the values
const resetAll = () => {
  billInput.value = ''
  tipInputs.forEach((tipInput) => {
    tipInput.classList.remove('active')
  })
  customTip.value = ''
  peopleInput.value = ''
  tipAmount.innerHTML = '$0.00'
  totalAmount.innerHTML = '$0.00'
  resetButton.classList.remove('btn-active')
}

billInput.addEventListener('input', calculateTip)
peopleInput.addEventListener('input', calculateTip)
resetButton.addEventListener('click', resetAll)

// Call the reset function on page load
resetAll()
